class Cart:
    def __init__(self, json=None):
        self.items = []
        if json is not None:
            for item in json['items']:
                self.items.append(item)

    def addItem(self, _item, _type, _quantity, _overwrite=False):
        _item = _item.toJson()
        found = False
        for item in self.items:
            if item['type'] == _type and item['data']['id'] == _item['id']:
                qty = int(_quantity)
                if not _overwrite:
                    qty += int(item['quantity'])
                if qty > 0:
                    item['quantity'] = qty
                found = True
                break
        if not found:
            self.items.append({
                'type': _type,
                'data': _item,
                'quantity': int(_quantity)
            })

    def subItem(self, _item, _type, _quantity):
        self.addItem(_item, _type, -int(_quantity))

    def removeItem(self, _item, _type):
        for item in self.items:
            if item['type'] == _type and item['data']['id'] == _item.id:
                self.items.remove(item)
                break

    def amount(self):
        price = 0
        for item in self.items:
            price += float(item['data']['price'])*float(item['quantity'])
        return price

    def toJson(self):
        return {'items': self.items}

    def count(self):
        return len(self.items)
