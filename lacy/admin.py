from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(Category)
admin.site.register(Staff)
admin.site.register(Product)
admin.site.register(Purchase)
admin.site.register(PurchaseProduct)
admin.site.register(EnterpriseService)
admin.site.register(PersonalService)
admin.site.register(Period)
admin.site.register(Subscription)
admin.site.register(UserServicePurchase)
admin.site.register(Project)