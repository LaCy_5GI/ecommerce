from .models import Category
from django.db.models import Count

# Context processor for category list
def add_category_list(request):
    category_list = Category.objects.annotate(nb_products=Count('product'))
    return {"category_list" : category_list}