import os

from Cryptodome.Cipher import PKCS1_OAEP
from Cryptodome.PublicKey import RSA
from base64 import b64decode
import datetime
import zlib


def decrypt_blob(encrypt_file, private_key_in, decrypt_name):
    # Import the Private Key and use for decryption using PKCS1_OAEP
    private_key = open(private_key_in, 'rb').read()
    rsakey = RSA.importKey(private_key)
    rsakey = PKCS1_OAEP.new(rsakey)

    encrypted_blob = open(encrypt_file, "rb").read()
    # Base 64 decode the data
    encrypted_blob = b64decode(encrypted_blob)

    # In determining the chunk size, determine the private key length used in bytes.
    # The data will be in decrypted in chunks
    chunk_size = 512
    offset = 0
    decrypted = b''

    # keep loop going as long as we have chunks to decrypt
    while offset < len(encrypted_blob):
        # The chunk
        chunk = encrypted_blob[offset: offset + chunk_size]

        # Append the decrypted chunk to the overall decrypted file
        decrypted += rsakey.decrypt(chunk)

        # Increase the offset by chunk size
        offset += chunk_size

    # return the decompressed decrypted data
    timestr = str(datetime.datetime.now().timestamp())
    fd = open(os.path.join(os.path.dirname(__file__), "decryptData", timestr+'decrypt_' + decrypt_name), "bw+")
    fd.write(zlib.decompress(decrypted))
    return os.path.join("decryptData", timestr+'decrypt_' + decrypt_name)