import os


from Cryptodome.Cipher import PKCS1_OAEP
from Cryptodome.PublicKey import RSA
from base64 import b64encode
from django.db.models.functions import datetime
import zlib

def encrypt_blob(input_file, real_name):
    print("dsfsdfsdffffffffff"+str(input_file))
    blob = open(input_file, "rb").read()
    # blob = input_file
    new_key = RSA.generate(4096)

    # The private key in PEM format
    private_key = new_key.exportKey("PEM")

    # The public key in PEM Format
    public_key = new_key.publickey().exportKey("PEM")
    real_path = str(datetime.datetime.now().timestamp())+'private_key.pem'
    fd = open(os.path.join(os.path.dirname(__file__), "encryptData",real_path), "bw+")
    fd.write(private_key)
    fd.close()
    # Import the Public Key and use for encryption using PKCS1_OAEP
    rsa_key = RSA.importKey(public_key)
    rsa_key = PKCS1_OAEP.new(rsa_key)

    # compress the data first
    blob = zlib.compress(blob)
    print('good')
    # In determining the chunk size, determine the private key length used in bytes
    # and subtract 42 bytes (when using PKCS1_OAEP). The data will be in encrypted
    # in chunks
    chunk_size = 470
    offset = 0
    end_loop = False
    encrypted = b''

    while not end_loop:
        # The chunk
        chunk = blob[offset:offset + chunk_size]

        # If the data chunk is less then the chunk size, then we need to add
        # padding with " ". This indicates the we reached the end of the file
        # so we end loop here
        if len(chunk) % chunk_size != 0:
            end_loop = True
            chunk += (" " * (chunk_size - len(chunk))).encode()

        # Append the encrypted chunk to the overall encrypted file
        print(b64encode(rsa_key.encrypt(chunk)).decode('utf-8'))
        encrypted += rsa_key.encrypt(chunk)

        # Increase the offset by chunk size
        offset += chunk_size

    # Base 64 encode the encrypted file
    fd = open(os.path.join(os.path.dirname(__file__), "encryptData", 'encrypt_' + real_name), "w+")
    fd.write(b64encode(encrypted).decode('utf-8'))
    fd.close()
    print(os.path.join("encryptData", 'encrypt_' + real_name))
    return os.path.join("encryptData", 'encrypt_' + real_name), os.path.join(
        "encryptData", real_path)
