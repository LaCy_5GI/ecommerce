from django.core.mail import EmailMultiAlternatives

from ECommerce.settings import SERVER_EMAIL


def html_message(email, message):
    return '<!doctype html> ' \
           '<html>' \
           '<head>' \
           '<meta name="viewport" content="width=device-width" />' \
           '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' \
           '<title>Simple Transactional Email</title>' \
           '<style>' \
           '/* -------------------------------------' \
           'GLOBAL RESETS' \
           '------------------------------------- */' \
           '/*All the styling goes here*/' \
           'img {' \
           'border: none;' \
           '-ms-interpolation-mode: bicubic;' \
           'max-width: 100%; ' \
           '}' \
           'body {' \
           'background-color: #f6f6f6;' \
           'font-family: sans-serif;' \
           '-webkit-font-smoothing: antialiased;' \
           'font-size: 14px;' \
           'line-height: 1.4;' \
           'margin: 0;' \
           'padding: 0;' \
           '-ms-text-size-adjust: 100%;' \
           '-webkit-text-size-adjust: 100%; ' \
           '}' \
           'table {' \
           'border-collapse: separate;' \
           'mso-table-lspace: 0pt;' \
           'mso-table-rspace: 0pt;' \
           'width: 100%; }' \
           'table td {' \
           'font-family: sans-serif;' \
           'font-size: 14px;' \
           'vertical-align: top; ' \
           '}' \
           '/* -------------------------------------' \
           'BODY & CONTAINER' \
           '------------------------------------- */' \
           '.body {' \
           'background-color: #f6f6f6;' \
           'width: 100%; ' \
           '}' \
           '/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */' \
           '.container {' \
           'display: block;' \
           'Margin: 0 auto !important;' \
           '/* makes it centered */' \
           'max-width: 580px;' \
           'padding: 10px;' \
           'width: 580px; ' \
           '}' \
           '/* This should also be a block element, so that it will fill 100% of the .container */' \
           '.content {' \
           'box-sizing: border-box;' \
           'display: block;' \
           'Margin: 0 auto;' \
           'max-width: 580px;' \
           'padding: 10px; ' \
           '}' \
           '/* -------------------------------------' \
           'HEADER, FOOTER, MAIN' \
           '------------------------------------- */' \
           '.main {' \
           'background: #ffffff;' \
           'border-radius: 3px;' \
           'width: 100%; ' \
           '}' \
           '.wrapper {' \
           'box-sizing: border-box;' \
           'padding: 20px; ' \
           '}' \
           '.content-block {' \
           'padding-bottom: 10px;' \
           'padding-top: 10px;' \
           '}' \
           '.footer {' \
           'clear: both;' \
           'Margin-top: 10px;' \
           'text-align: center;' \
           'width: 100%; ' \
           '}' \
           '.footer td,' \
           '.footer p,' \
           '.footer span,' \
           '.footer a {' \
           'color: #999999;' \
           'font-size: 12px;' \
           'text-align: center; ' \
           '}' \
           '/* -------------------------------------' \
           'TYPOGRAPHY' \
           '------------------------------------- */' \
           'h1,' \
           'h2,' \
           'h3,' \
           'h4 {' \
           'color: #000000;' \
           'font-family: sans-serif;' \
           'font-weight: 400;' \
           'line-height: 1.4;' \
           'margin: 0;' \
           'margin-bottom: 30px; ' \
           '}' \
           'h1 {' \
           'font-size: 35px;' \
           'font-weight: 300;' \
           'text-align: center;' \
           'text-transform: capitalize; ' \
           '}' \
           'p, ul, ol {' \
           'font-family: sans-serif;' \
           'font-size: 14px;' \
           'font-weight: normal;' \
           'margin: 0;' \
           'margin-bottom: 15px; ' \
           '}' \
           'p li,' \
           'ul li,' \
           'ol li {' \
           'list-style-position: inside;' \
           'margin-left: 5px;' \
           '}' \
           'a {' \
           'color: #3498db;' \
           'text-decoration: underline; ' \
           '}' \
           '.last {' \
           'margin-bottom: 0; ' \
           '}' \
           '.first {' \
           'margin-top: 0; ' \
           '}' \
           '.align-center {' \
           'text-align: center; ' \
           '}' \
           '.align-right {' \
           'text-align: right; ' \
           '}' \
           '.align-left {' \
           'text-align: left; ' \
           '}' \
           '.clear {' \
           'clear: both; ' \
           '}' \
           '.mt0 {' \
           'margin-top: 0; ' \
           '}' \
           '.mb0 {' \
           'margin-bottom: 0; ' \
           '}' \
           '.preheader {' \
           'color: transparent;' \
           'display: none;' \
           'height: 0;' \
           'max-height: 0;' \
           'max-width: 0;' \
           'opacity: 0;' \
           'overflow: hidden;' \
           'mso-hide: all;' \
           'visibility: hidden;' \
           'width: 0; ' \
           '}' \
           '.powered-by a {' \
           'text-decoration: none; ' \
           '}' \
           'hr {' \
           'border: 0;' \
           'border-bottom: 1px solid #f6f6f6;' \
           'Margin: 20px 0;' \
           '}' \
           '/* -------------------------------------' \
           'RESPONSIVE AND MOBILE FRIENDLY STYLES' \
           '------------------------------------- */' \
           '@media only screen and (max-width: 620px) {' \
           'table[class=body] h1 {' \
           'font-size: 28px !important;' \
           'margin-bottom: 10px !important;' \
           '}' \
           'table[class=body] p,' \
           'table[class=body] ul,' \
           'table[class=body] ol,' \
           'table[class=body] td,' \
           'table[class=body] span,' \
           'table[class=body] a {' \
           'font-size: 16px !important;' \
           '}' \
           'table[class=body] .wrapper,' \
           'table[class=body] .article {' \
           'padding: 10px !important;' \
           '}' \
           'table[class=body] .content {' \
           'padding: 0 !important;' \
           '}' \
           'table[class=body] .container {' \
           'padding: 0 !important;' \
           'width: 100% !important;' \
           '}' \
           'table[class=body] .main {' \
           'border-left-width: 0 !important;' \
           'border-radius: 0 !important;' \
           'border-right-width: 0 !important;' \
           '}' \
           'table[class=body] .btn table {' \
           'width: 100% !important;' \
           '}' \
           'table[class=body] .btn a {' \
           'width: 100% !important;' \
           '}' \
           'table[class=body] .img-responsive {' \
           'height: auto !important;' \
           'max-width: 100% !important;' \
           'width: auto !important;' \
           '}' \
           '}' \
           '/* -------------------------------------' \
           'PRESERVE THESE STYLES IN THE HEAD' \
           '------------------------------------- */' \
           '@media all {' \
           '.ExternalClass {' \
           'width: 100%;' \
           '}' \
           '.ExternalClass,' \
           '.ExternalClass p,' \
           '.ExternalClass span,' \
           '.ExternalClass font,' \
           '.ExternalClass td,' \
           '.ExternalClass div {' \
           'line-height: 100%;' \
           '}' \
           '.apple-link a {' \
           'color: inherit !important;' \
           'font-family: inherit !important;' \
           'font-size: inherit !important;' \
           'font-weight: inherit !important;' \
           'line-height: inherit !important;' \
           'text-decoration: none !important;' \
           '}' \
           '.btn-primary table td:hover {' \
           'background-color: #34495e !important;' \
           '}' \
           '.btn-primary a:hover {' \
           'background-color: #34495e !important;' \
           'border-color: #34495e !important;' \
           '}' \
           '}' \
           '</style>' \
           '</head>' \
           '<body class="">' \
           '<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">' \
           '<tr>' \
           '<td>&nbsp;</td>' \
           '<td class="container">' \
           '<div class="content">' \
           '<!-- START CENTERED WHITE CONTAINER -->' \
           '<span class="preheader">This is preheader text. Some clients will show this text as a preview.</span>' \
           '<table role="presentation" class="main">' \
           '<!-- START MAIN CONTENT AREA -->' \
           '  <tr>' \
           '    <td class="wrapper">' \
           '      <table role="presentation" border="0" cellpadding="0" cellspacing="0">' \
           '        <tr>' \
           '          <td>' \
           '            <p>Hi there,</p>' \
           '            <p>Somebody contact-us for the specified email subject</p>' \
           '            <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">' \
                                                                   '<tr>' \
                                                                   '                  <td align="left">' \
                                                                   '<b>Email</b>' \
                                                                   '</td>' \
                                                                   '<td align="left">' \
                                                                   '' + email + '' \
                                                                                '</td>' \
                                                                                '</tr>' \
                                                                                '</table>' \
                                                                                '<p><b>Message</b> : ' + message + '.</p>' \
                                                                                                                   '<p>Thanks, LACY</p>' \
                                                                                                                   '</td>' \
                                                                                                                   '</tr>' \
                                                                                                                   '</table>' \
                                                                                                                   '</td>' \
                                                                                                                   '</tr>' \
                                                                                                                   '<!-- END MAIN CONTENT AREA -->' \
                                                                                                                   '</table>' \
                                                                                                                   '<!-- START FOOTER -->' \
                                                                                                                   '<div class="footer">' \
                                                                                                                   '<table role="presentation" border="0" cellpadding="0" cellspacing="0">' \
                                                                                                                   '<tr>' \
                                                                                                                   '<td class="content-block">' \
                                                                                                                   '<span class="apple-link">LACY, GI ENSP</span>' \
                                                                                                                   '</td>' \
                                                                                                                   '</tr>' \
                                                                                                                   '<tr>' \
                                                                                                                   '<td class="content-block powered-by">' \
                                                                                                                   'Powered by <a href="http://localhost/lacy">us</a>.' \
                                                                                                                   '</td>' \
                                                                                                                   '</tr>' \
                                                                                                                   '</table>' \
                                                                                                                   '</div>' \
                                                                                                                   '<!-- END FOOTER -->' \
                                                                                                                   '<!-- END CENTERED WHITE CONTAINER -->' \
                                                                                                                   '</div>' \
                                                                                                                   '</td>' \
                                                                                                                   '<td>&nbsp;</td>' \
                                                                                                                   '</tr>' \
                                                                                                                   '</table>' \
                                                                                                                   '</body>' \
                                                                                                                   '</html>'


def html_message_registration(user_id, name):
    return \
'   <html>'\
'   <head>'\
'   <style>'\
'      .banner-color {'\
'     background-color: #eb681f;'\
'        }'\
'        .title-color {'\
'        color: #0066cc;'\
'        }'\
'        .button-color {'\
'        background-color: #0066cc;'\
'        }'\
'        @media screen and (min-width: 500px) {'\
'    .banner-color {'\
'        background-color: #0066cc;'\
'    }'\
'        .title-color {'\
'        color: #eb681f;'\
'        }'\
'        .button-color {'\
'        background-color: #eb681f;'\
'        }'\
'        }'\
'     </style>'\
'  </head>'\
'  <body>'\
'     <div style="background-color:#ececec;padding:0;margin:0 auto;font-weight:200;width:100%!important">'\
'        <table align="center" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">'\
'           <tbody>'\
'              <tr>'\
'                 <td align="center">'\
'                    <center style="width:100%">'\
'                       <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="margin:0 auto;max-width:512px;font-weight:200;width:inherit;font-family:Helvetica,Arial,sans-serif" width="512">'\
'                          <tbody>'\
'                             <tr>'\
'                                <td bgcolor="#F3F3F3" width="100%" style="background-color:#f3f3f3;padding:12px;border-bottom:1px solid #ececec">'\
'                                   <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;width:100%!important;font-family:Helvetica,Arial,sans-serif;min-width:100%!important" width="100%">'\
'                                      <tbody>'\
'                                         <tr>'\
'                                            <td align="left" valign="middle" width="50%"><span style="margin:0;color:#4c4c4c;white-space:normal;display:inline-block;text-decoration:none;font-size:12px;line-height:20px">Lacy</span></td>'\
'                                            <td valign="middle" width="50%" align="right" style="padding:0 0 0 10px"></td>'\
'                                            <td width="1">&nbsp;</td>'\
'                                         </tr>'\
'                                      </tbody>'\
'                                   </table>'\
'                                </td>'\
'                             </tr>'\
'                             <tr>'\
'                                <td align="left">'\
'                                   <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">'\
'                                      <tbody>'\
'                                        <tr>'\
'                                            <td width="100%">'\
'                                               <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">'\
'                                                  <tbody>'\
'                                                     <tr>'\
'                                                        <td align="center" bgcolor="#8BC34A" style="padding:20px 48px;color:#ffffff" class="banner-color">'\
'                                                           <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">'\
'                                                              <tbody>'\
'                                                                 <tr>'\
'                                                                    <td align="center" width="100%">'\
'                                                                       <h1 style="padding:0;margin:0;color:#ffffff;font-weight:500;font-size:20px;line-height:24px">Confirm your account</h1>'\
'                                                                    </td>'\
'                                                                 </tr>'\
'                                                              </tbody>'\
'                                                           </table>'\
'                                                        </td>'\
'                                                     </tr>'\
'                                                     <tr>'\
'                                                        <td align="center" style="padding:20px 0 10px 0">'\
'                                                           <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">'\
'                                                              <tbody>'\
'                                                                 <tr>'\
'                                                                    <td align="center" width="100%" style="padding: 0 15px;text-align: justify;color: rgb(76, 76, 76);font-size: 12px;line-height: 18px;">'\
'                                                                      <h3 style="font-weight: 600; padding: 0px; margin: 0px; font-size: 16px; line-height: 24px; text-align: center;" class="title-color">Hi '+name+',</h3>'\
'                                                                       <p style="margin: 20px 0 30px 0;font-size: 15px;text-align: center;">Confirm your email address to complete your account. It\'s easy... <b>Just click the button below</b>!</p>'\
'                                                                       <div style="font-weight: 200; text-align: center; margin: 25px;"><a href="http://localhost:8000/lacy/confirm_registration/'+str(user_id)+'" style="padding:0.6em 1em;border-radius:600px;color:#ffffff;font-size:14px;text-decoration:none;font-weight:bold" class="button-color">Confirm now</a></div>'\
'                                                                    </td>'\
'                                                                 </tr>'\
'                                                              </tbody>'\
'                                                           </table>'\
'                                                        </td>'\
'                                                     </tr>'\
'                                                     <tr>'\
'                                                     </tr>'\
'                                                     <tr>'\
'                                                     </tr>'\
'                                                  </tbody>'\
'                                               </table>'\
'                                            </td>'\
'                                         </tr>'\
'                                      </tbody>'\
'                                   </table>'\
'                                </td>'\
'                             </tr>'\
'                             <tr>'\
'                                <td align="left">'\
'                                   <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="padding:0 24px;color:#999999;font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">'\
'                                      <tbody>'\
'                                         <tr>'\
'                                            <td align="center" width="100%">'\
'                                               <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">'\
'                                                  <tbody>'\
'                                                     <tr>'\
'                                                        <td align="center" valign="middle" width="100%" style="border-top:1px solid #d9d9d9;padding:12px 0px 20px 0px;text-align:center;color:#4c4c4c;font-weight:200;font-size:12px;line-height:18px">Powered by,'\
'                                                           <br><b>National School Advanced of engeneering of Cameroon</b>'\
'                                                        </td>'\
'                                                     </tr>'\
'                                                  </tbody>'\
'                                               </table>'\
'                                            </td>'\
'                                         </tr>'\
'                                         <tr>'\
'                                            <td align="center" width="100%">'\
'                                               <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">'\
'                                                  <tbody>'\
'                                                     <tr>'\
'                                                       <td align="center" style="padding:0 0 8px 0" width="100%"></td>'\
'                                                     </tr>'\
'                                                  </tbody>'\
'                                               </table>'\
'                                            </td>'\
'                                         </tr>'\
'                                      </tbody>'\
'                                   </table>'\
'                                </td>'\
'                             </tr>'\
'                          </tbody>'\
'                       </table>'\
'                    </center>'\
'                 </td>'\
'              </tr>'\
'           </tbody>'\
'        </table>'\
'     </div>'\
'</body>'\
'</html>'\


def send_mail_registration(to, user_id, name):
    try:
        subject = 'Registration'
        text_content = 'Confirm your account.'
        msg = EmailMultiAlternatives(subject, text_content, SERVER_EMAIL, [to])
        msg.attach_alternative(html_message_registration(user_id, name), "text/html")
        msg.send()
        return True
    except:
        return False
