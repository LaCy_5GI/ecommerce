from django.core.management.base import BaseCommand,  CommandError
from lacy.models import UserServicePurchase
import datetime

class Command(BaseCommand):
    help = 'The command drop entries from UserServicePurchase when their end_date is passed'

    def handle(self, *args, **options):
        UserServicePurchase.objects.filter(end_date__gte=datetime.datetime.now()).delete()