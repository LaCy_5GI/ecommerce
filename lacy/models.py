from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model

class Category(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.name


class Staff(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to="products", blank=True, null=True)
    description = models.TextField()
    price = models.FloatField(default=0.0)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return self.name + " " + str(self.price) + " FCFA"

    def toJson(self):
        return {'id': self.id, 'name': self.name, 'image': self.image.url, 'price': float(self.price)}


class Service(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    image = models.ImageField(upload_to="services", blank=True, null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class EnterpriseService(Service):
    pass


class PersonalService(Service):
    price = models.FloatField(default=0.0)

    def toJson(self):
        return {'id': self.id, 'name': self.name, 'image': self.image.url, 'price': float(self.price)}


class Period(models.Model):
    duration = models.IntegerField(default=7)


class Subscription(models.Model):
    period = models.ForeignKey(Period, on_delete=models.CASCADE)
    service = models.ForeignKey(PersonalService, on_delete=models.CASCADE)
    price = models.FloatField(default=0.0)


class UserServicePurchase(models.Model):
    service = models.ForeignKey(PersonalService, on_delete=models.CASCADE)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()

    def __str__(self):
        return self.user.first_name + " " + self.service.name \
               + "[" + str(self.start_date) + " - " + str(self.end_date) + "]"


class Project(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    startDate = models.DateTimeField(blank=True)
    endDate = models.DateTimeField()
    project_manager = models.ForeignKey(Staff, on_delete=models.CASCADE)
    INPROGRESS = 'INPROGRESS'
    FINISH = 'FINISH'
    PLANNED = 'PLANNED'
    STATUS_CHOICES = (
        (INPROGRESS, 'InProgress'),
        (FINISH, 'Finish'),
        (PLANNED, 'Planned'),
    )
    status = models.CharField(
        max_length=15,
        choices=STATUS_CHOICES,
        default=INPROGRESS
    )
    budget = models.FloatField(default=0)

    def __str__(self):
        return self.title


class Purchase(models.Model):
    amount = models.FloatField(default=0.0)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now())


class PurchaseProduct(models.Model):
    purchase = models.ForeignKey(Purchase, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=0)


