import os
import json

import django
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, HttpResponseRedirect, get_object_or_404, HttpResponse, redirect, render_to_response
from django.http import FileResponse
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt

from ECommerce.settings import SERVER_EMAIL
from lacy.Cart import Cart
from lacy.helpers.mailing import send_mail_registration
from .models import *
from django.core.mail import send_mail
from .helpers.mailing import html_message
from django.db.models import Count
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from .helpers.encrypt import encrypt_blob
from .helpers.decrypt import decrypt_blob
import datetime
import requests
import urllib
import urllib.request as urllib2
from paypal.standard.forms import PayPalPaymentsForm
from django.urls import reverse

# Create your views here.

def handler404(request):
    return render(request, 'lacy/404.html',)

def home(request):
    #personal services
    return render(request, 'lacy/index.html',)

def products(request, category_id):
    product_list = Product.objects.filter( category__id = category_id )
    return render(request, 'lacy/products.html', {"product_list": product_list, "theCategory": category_id})

def services(request, category_id):
    number1 = EnterpriseService.objects.all().count()
    number2 = PersonalService.objects.all().count()
    if category_id == 0:
        service_list = EnterpriseService.objects.all()
    else:
        service_list = PersonalService.objects.all()
    return render(request, 'lacy/services.html', {"service_list": service_list,
                                                  "category": category_id,
                                                  "number1":number1,
                                                  "number2":number2,})



def contact(request):
    if request.method == 'POST':
        form = request.POST
        first_name = form['first-name']
        last_name = form['last-name']
        subject = form['subject']
        email = form['email']
        message = form['message']
        name = first_name + ' ' + last_name

        # construction du message html qui sera envoyé
        infos_name = "<br><b>Name</b> :" + name + "<br>"
        if send_mail(subject, message, SERVER_EMAIL,
                     recipient_list=[SERVER_EMAIL, ],
                     html_message=html_message(email, message + infos_name), fail_silently=False):
            return render(request, 'lacy/contact.html', {'success_mail': True,})
    return render(request, 'lacy/contact.html', {'success_mail': False,})



def team(request):
    return render(request, 'lacy/team.html',)

def cart(request):
    category_list = Category.objects.annotate(nb_products=Count('product'))
    return render(request, 'lacy/shop-basket.html', {
        "category_list": category_list,
        "cart": getCart(request)
    })


def checkout_1(request):
    return render(request, 'lacy/shop-checkout1.html',)

def checkout_2(request):
    return render(request, 'lacy/shop-checkout2.html',)

def checkout_3(request):
    return render(request, 'lacy/shop-checkout3.html',)

def checkout_4(request):
    return render(request, 'lacy/shop-checkout4.html',)

def product_detail(request, product_id):
    product = get_object_or_404(Product, pk=product_id)
    return render(request, 'lacy/shop-detail.html', {"product": product})

def service_detail(request, category_id, service_id):
    service_subscriptions = None
    if category_id == 0:
        service = get_object_or_404(EnterpriseService, pk=service_id)
        others_services = EnterpriseService.objects.all()
    else:
        service = get_object_or_404(PersonalService, pk=service_id)
        others_services = PersonalService.objects.all()
        service_subscriptions = Subscription.objects.filter(service__id=service_id)

    return render(request, 'lacy/service-detail.html', {'service':service,
                                                        'category':category_id,
                                                        'others_services':others_services,
                                                        'subscriptions':service_subscriptions,})


def getCart(request):
    return Cart(request.session.get('cart', None))


def storeCart(request, cart):
    request.session['cart'] = cart.toJson()


def addToCart(request):
    if request.method == 'POST':
        cart = getCart(request)
        data = request.POST

        if data['item_type'] == 'PRODUCT':
            item = Product.objects.get(id=data['item_id'])
        else:
            item = PersonalService.objects.get(id=data['item_id'])

        cart.addItem(item, data['item_type'], data['quantity'])
        storeCart(request, cart)
    return HttpResponseRedirect(reverse('lacy:cart'))


def subFromCart(request):
    if request.method == 'POST':
        cart = getCart(request)
        data = request.POST

        if data['item_type'] == 'PRODUCT':
            item = Product.objects.get(id=data['item_id'])
        else:
            item = PersonalService.objects.get(id=data['item_id'])

        cart.subItem(item, data['item_type'], data['quantity'])
        storeCart(request, cart)
    return HttpResponseRedirect(reverse('lacy:cart'))


def removeFromCart(request, type, id):
    if request.method == 'GET':
        cart = getCart(request)

        if type == 'PRODUCT':
            item = Product.objects.get(id=id)
        else:
            item = PersonalService.objects.get(id=id)

        cart.removeItem(item, type)

        storeCart(request, cart)
    return HttpResponseRedirect(reverse('lacy:cart'))


def clearCart(request):
    if request.method == 'POST':
        storeCart(request, Cart())
    return HttpResponseRedirect(reverse('lacy:cart'))


def updateCart(request):
    if request.method == 'POST':
        cart = getCart(request)
        data = request.POST

        for key in data:
            parts = key.split('-')
            if len(parts) == 2:
                type = str(parts[0])
                id = parts[1]
                qty = data[key]
                if type == 'PRODUCT':
                    item = Product.objects.get(id=id)
                else:
                    item = PersonalService.objects.get(id=id)

                cart.addItem(item, type, qty, True)

        storeCart(request, cart)
    return HttpResponseRedirect(reverse('lacy:cart'))


def cartCheckout(request):
    if request.method == 'POST':
        cart = getCart(request)
        purchase = Purchase(user=request.user, amount=cart.amount())
        purchase.save()
        for product in cart.items:
            PurchaseProduct(purchase=purchase, product=product['product'], quantity=product['quantity']).save()
        clearCart(request)
    return HttpResponseRedirect(reverse('lacy:cart'))


"""
def cart(request):
    personal_services, enterprise_services = personal_enterprise_service_formatting()
    return render(request, 'lacy/cart.html', {'cart': getCart(request),
                                              "enterprise_services": enterprise_services,
                                              "personal_services": personal_services})

def payViaMomo(request):
    # Url Api de paiement MonetBil
    url = "https://api.monetbil.com/widget/v2.1/Je0J3wOesnqdkuRx4lIpBYipc1mkObGH"
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
    }

    data = {"amount": "500", "currency": "XAF",
            "item_ref": "Item_bought", "locale": "fr", "country": "CM",
            "first_name": "Samaritain", "last_name": "Sims",
            "phone": "697266488", "email": "s@s.s",
            "return_url": "http://127.0.0.1/lacy/momopaymentsuccess",
            "notify_url": "http://127.0.0.1/lacy/feedbackmomo"}
    # Envoi de la requete de paiement
    res = requests.post(url, headers=headers, data= urllib.parse.urlencode(data))
    # Obtention et parsage de la reponse
    res_json = res.json()
    print(res_json)

    if res_json is None:  # Reponse NULL
        return HttpResponse("""
"""<html>
                <head>
                    <meta charset="utf-8">
                    <title>Error</title>
                </head>
                <body>
                    <pre> Res null </pre>
                </body>
            <html>
"""
"""% json.dump(res_json, 2))
    else:
        if 'payment_url' not in res_json:  # Url du paiement absent dans la reponse
            return HttpResponse("""
"""<html>
                <head>
                    <meta charset="utf-8">
                    <title>Error - Payment url not sent</title>
                </head>
                <body>
                    <pre> %s </pre>
                </body>
            <html>
                            """
""")
        else:  # Reponse correcte, on redirige l'utilisateur vers l'url de paiement
            return redirect(res_json['payment_url'])


def checkMoMoPaymentStatus(request, paymentId):
    data = urllib.urlencode({"paymentId": paymentId})
    headers = {'Content-Type': 'application/json'}
    req = urllib2.Request("POST https://api.monetbil.com/payment/v1/placePayment", data=data, headers=headers);
    res = json.loads(req.read());

    sres = json.dump(res, 2)
    return render(request, 'lacy/see_momo_payment.html', sres);

def payViaPayPal(request):
    # What you want the button to do.
    paypal_dict = {
        "business": "sfaysamaritan@gmail.com", #Adresse email du marchand, a l'occurence celle de LACY
        "amount": "100.00", #Montant
        "item_name": "name of the item", #Reference de la commande / du produit
        "invoice": "unique-invoice-id", #NE PAS CHANGER CA
        "notify_url": request.build_absolute_uri(django.urls.reverse('paypal-ipn')), #Url de callback - SIMILAIRE AU CAS DU PAIEMENT PAR MOMO
        "return": request.build_absolute_uri(django.urls.reverse('paypal_success')), #Url de redirection
        "cancel_return": request.build_absolute_uri(django.urls.reverse('paypal_error')), #Url de redirection en cas d'annulation volontaire de l'utilisateur
        "custom": "premium_plan",  # Custom command to correlate to some function later (optional)
    }
    # Create the instance.
    # Creation du formulaire de payment a partir des donnees sur le paiement
    form = PayPalPaymentsForm(initial=paypal_dict)
    #Envoie de ce formulaire a la vue pour generer le bouton de paiemnt, Le bouton de paiement va rediriger le client vers le site de Paypal pour completer le paiement
    context = {"form": form}
    return render(request, "lacy/paypal_payment.html", context)

@csrf_exempt
def paypalReturnSuccess(request):
    return render(request, "lacy/paypal_success.html", locals())

@csrf_exempt
def paypalReturnError(request):
    return render(request, "lacy/paypal_error.html", locals())



def getCart(request):
    personal_services, enterprise_services = personal_enterprise_service_formatting()
    return Cart(request.session.get('cart', {'products': [],
                                             "enterprise_services": enterprise_services,
                                             "personal_services": personal_services
                                             }))


def storeCart(request, cart):
    request.session['cart'] = cart.toJson()


def addToCart(request):
    if request.method == 'POST':
        cart = getCart(request)
        data = request.POST
        cart.addProduct(Product.objects.get(id=data['product_id']), data['quantity'])
        storeCart(request, cart)
    return HttpResponseRedirect(reverse('lacy:cart'))


def subFromCart(request):
    if request.method == 'POST':
        cart = getCart(request)
        data = request.POST
        cart.subProduct(Product.objects.get(id=data['product_id']), data['quantity'])
        storeCart(request, cart)
    return HttpResponseRedirect(reverse('lacy:cart'))


def removeFromCart(request):
    if request.method == 'POST':
        cart = getCart(request)
        data = request.POST
        cart.removeProduct(Product.objects.get(id=data['product_id']))
        storeCart(request, cart)
    return HttpResponseRedirect(reverse('lacy:cart'))


def clearCart(request):
    if request.method == 'POST':
        storeCart(request, Cart())
    return HttpResponseRedirect(reverse('lacy:cart'))


def cartCheckout(request):
    if request.method == 'POST':
        cart = getCart(request)
        purchase = Purchase(user=request.user, amount=cart.amount())
        purchase.save()
        for product in cart.products:
            PurchaseProduct(purchase=purchase, product=product['product'], quantity=product['quantity']).save()
        clearCart(request)
    return HttpResponseRedirect(reverse('lacy:cart'))


def verification(request, service):
    current_user = request.user
    print("ici")
    try:
        sub = UserServicePurchase.objects.get(service=service, user=current_user)
    except  UserServicePurchase.DoesNotExist:
        sub = None
    print(sub)
    if sub is None:
        print("voilà")
        return redirect('lacy:subscriptions', service_id=service.id)
    else :
        return None
"""